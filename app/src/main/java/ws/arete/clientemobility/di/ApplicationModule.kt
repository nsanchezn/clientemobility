package ws.arete.clientemobility.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

/**
 * @author Created by normansanchez on 6/26/19.
 */
@Module
abstract class ApplicationModule {
    //expose Application as an injectable context

    @Binds
    internal abstract fun bindContext(application: Application): Context
}