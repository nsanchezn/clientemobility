package ws.arete.clientemobility.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ws.arete.clientemobility.ClienteMobilityApplication
import ws.arete.clientemobility.util.UtilsModule
import javax.inject.Singleton

/**
 * @author Created by normansanchez on 6/26/19.
 */
@Singleton
@Component(modules = [(ApplicationModule::class), (ActivityBindingModule::class),
    (UtilsModule::class), (AndroidSupportInjectionModule::class)])
interface AppComponent: AndroidInjector<ClienteMobilityApplication> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}