package ws.arete.clientemobility.di

import javax.inject.Scope

/**
 * @author Created by normansanchez on 6/26/19.
 */
@Scope
@Retention
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class FragmentScoped
