package ws.arete.clientemobility.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ws.arete.clientemobility.login.LoginActivity
import ws.arete.clientemobility.login.LoginModule

/**
 * @author Created by normansanchez on 6/26/19.
 */

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [(LoginModule::class)])
    internal abstract fun loginActivity(): LoginActivity

}