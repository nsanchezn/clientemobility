package ws.arete.clientemobility.login

import com.google.firebase.auth.FirebaseAuth

/**
 * @author Created by normansanchez on 6/26/19.
 */
class LoginAuthListener(private val authCallback: LoginContract.AuthCallback)
    : FirebaseAuth.AuthStateListener {

    override fun onAuthStateChanged(p0: FirebaseAuth) {
        val user = p0.currentUser
        if (user != null){
            authCallback.onAuthSuccess(user.uid)
        }
    }
}