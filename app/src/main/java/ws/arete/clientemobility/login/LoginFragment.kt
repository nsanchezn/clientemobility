package ws.arete.clientemobility.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment
import ws.arete.clientemobility.R
import javax.inject.Inject

/**
 * @author Created by normansanchez on 6/26/19.
 */
class LoginFragment @Inject constructor() : DaggerFragment(),LoginContract.View{
    @Inject
    lateinit var mPresenter: LoginContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}