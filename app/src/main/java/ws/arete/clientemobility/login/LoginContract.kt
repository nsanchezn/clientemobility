package ws.arete.clientemobility.login

import ws.arete.clientemobility.BasePresenter
import ws.arete.clientemobility.BaseView

/**
 * @author Created by normansanchez on 6/26/19.
 */
interface LoginContract {

    interface View : BaseView<Presenter> {

    }

    interface Presenter : BasePresenter<View>{

    }

    interface AuthInteractor{
        fun startAuthListening(authCallback: AuthCallback)
        fun signInWithCustomToken(token: String)
    }

    interface AuthCallback{
        fun onAuthSuccess(uid: String)
    }

    interface LoginApiInteractor{
        fun grantAccess(empresa: String, password: String, loginApiCallback: LoginApiCallback)
    }

    interface LoginApiCallback{
        fun onApiSuccess(token:String, idCedi:String, cediEmpresa:String, fletera: String)
        fun onApiFailure()
    }

}