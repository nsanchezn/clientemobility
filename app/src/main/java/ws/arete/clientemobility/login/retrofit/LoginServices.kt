package ws.arete.clientemobility.login.retrofit

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import ws.arete.clientemobility.BuildConfig
/**
 * Created by normansanchez on 25/06/2019.
 */
interface LoginServices {

    @Headers(BuildConfig.X_API_KEY)
    @POST("/" + BuildConfig.FirebaseURL + "v3/mobility/login")
    fun grantAccess(@Body loginData: BodyLoginEmpresa): Call<ResponseBody>
}