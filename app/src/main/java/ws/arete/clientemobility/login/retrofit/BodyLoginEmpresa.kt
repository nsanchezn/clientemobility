package ws.arete.clientemobility.login.retrofit

/**
 * Created by normansanchez on 25/06/2019.
 */
data class BodyLoginEmpresa (val cedi: String , val password: String)