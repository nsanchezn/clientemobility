package ws.arete.clientemobility.login

import android.content.pm.ActivityInfo
import android.os.Bundle
import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity
import ws.arete.clientemobility.R
import ws.arete.clientemobility.util.ActivityUtils
import ws.arete.clientemobility.util.BackPressedCounter
import javax.inject.Inject

/**
 * @author Created by normansanchez on 6/26/19.
 */
class LoginActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var loginFragmentProvider: Lazy<LoginFragment>
    @Inject
    lateinit var backPressCounterProvider: Lazy<BackPressedCounter>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_home)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        var loginFragment: androidx.fragment.app.Fragment? =
                supportFragmentManager.findFragmentById(R.id.fragment_content)
        if (loginFragment == null) {
            loginFragment = loginFragmentProvider.get()
            ActivityUtils.addFragmentToActivity(
                    supportFragmentManager, loginFragment, R.id.fragment_content
            )
        }
    }

    override fun onBackPressed() { backPressCounterProvider.get().checkLastPress() }

}
