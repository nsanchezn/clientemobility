package ws.arete.clientemobility.login.retrofit

import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by normansanchez on 25/06/2019.
 */
class LoginApiHelper private constructor(){
    companion object {
        private const val BASE_URL = "https://b2uiut18x9.execute-api.us-east-1.amazonaws.com"
        @JvmField val  httpClient: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build()
        @JvmField var instance: LoginApiHelper = LoginApiHelper()
    }
    private var  service:LoginServices

    private fun createAdapter():Retrofit.Builder {

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)

    }

    init {

        val retrofit = createAdapter().build()
        service = retrofit.create(LoginServices::class.java)
    }

    fun grantAccess(body:BodyLoginEmpresa): Call<ResponseBody> {
        return service.grantAccess(body)
    }
}