package ws.arete.clientemobility.login

import android.util.Log
import com.google.firebase.perf.metrics.AddTrace
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import ws.arete.clientemobility.login.retrofit.BodyLoginEmpresa
import ws.arete.clientemobility.login.retrofit.LoginApiHelper
import javax.inject.Inject

/**
 * @author Created by normansanchez on 6/26/19.
 */
class LoginApiInteractor @Inject constructor(): LoginContract.LoginApiInteractor {
    @AddTrace(name = "inicio_de_sesion", enabled = true)
    override fun grantAccess(empresa: String, password: String, loginApiCallback: LoginContract.LoginApiCallback) {
        LoginApiHelper.instance
                .grantAccess(BodyLoginEmpresa(empresa,password))
                .enqueue(object : retrofit2.Callback<ResponseBody> {

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        Log.d("onFailure: ", t?.message)
                        loginApiCallback.onApiFailure()
                    }

                    @AddTrace(name = "tiempo_respuesta_API_login", enabled = true)
                    override fun onResponse(call: Call<ResponseBody>?,
                                            response: Response<ResponseBody>?) {
                        try {
                            val resp = response?.body()?.string()
                            val jsonResponse = JSONObject(resp)
                            val token = jsonResponse.getString("token")
                            val idCedi = jsonResponse.getJSONObject("resultado")
                            if (token.isEmpty()) {
                                loginApiCallback.onApiFailure()
                            } else {
                                loginApiCallback.onApiSuccess(token,
                                        idCedi.getString("idCedi")
                                        ,idCedi.getString("Cedi"),
                                        idCedi.getString("fletera"))
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            Log.d("Exception: ", ex.message)
                            loginApiCallback.onApiFailure()
                        }

                    }
                })
    }
}