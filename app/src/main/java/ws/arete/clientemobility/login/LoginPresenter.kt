package ws.arete.clientemobility.login

import javax.inject.Inject

/**
 * @author Created by normansanchez on 6/26/19.
 */
class LoginPresenter @Inject constructor():
    LoginContract.Presenter{

    private var mView: LoginContract.View? = null

    override fun takeView(view: LoginContract.View) {
        mView = view
    }

    override fun dropView() {
        mView = null
    }
}