package ws.arete.clientemobility.login

import com.google.firebase.auth.FirebaseAuth
import javax.inject.Inject

/**
 * @author Created by normansanchez on 6/26/19.
 */
class LoginAuthInteractor @Inject constructor()
    : LoginContract.AuthInteractor {

    override fun startAuthListening(authCallback: LoginContract.AuthCallback) {
        FirebaseAuth.getInstance().addAuthStateListener(LoginAuthListener(authCallback))
    }

    override fun signInWithCustomToken(token: String) {
        FirebaseAuth.getInstance().signInWithCustomToken(token)
    }
}