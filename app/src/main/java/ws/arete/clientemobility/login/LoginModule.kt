package ws.arete.clientemobility.login

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import ws.arete.clientemobility.di.ActivityScoped
import ws.arete.clientemobility.di.FragmentScoped

/**
 * @author Created by normansanchez on 6/26/19.
 */
@Module
abstract class LoginModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun loginFragment(): LoginFragment

    @ActivityScoped
    @Binds
    internal abstract fun loginPresenter(presenter: LoginPresenter) : LoginContract.Presenter

    @ActivityScoped
    @Binds
    internal abstract fun loginAuthInteractor(authInteractor: LoginAuthInteractor): LoginContract.AuthInteractor

    @ActivityScoped
    @Binds
    internal abstract fun loginApiInteractor(loginApiInteractor: LoginApiInteractor): LoginContract.LoginApiInteractor
}