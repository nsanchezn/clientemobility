package ws.arete.clientemobility.misc;

/**
 * @author Created by normansanchez on 6/26/19.
 */
public interface FragmentLoginInteractionListener {
    void manageLogin(String comesFrom);
}
