package ws.arete.clientemobility.misc;

import android.location.Location;

/**
 * @author Created by normansanchez on 6/26/19.
 */
public interface FragmentLocationInteractionListener {
    String getLongitude();
    String getLatitude();
    Location getLocation();
    boolean getLocationAvailability();
}
