package ws.arete.clientemobility

import com.crashlytics.android.Crashlytics
import com.google.firebase.database.FirebaseDatabase
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.fabric.sdk.android.Fabric
import ws.arete.clientemobility.di.DaggerAppComponent

/**
 * @author Created by normansanchez on 6/26/19.
 */
class ClienteMobilityApplication : DaggerApplication(){

    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        val fabric = Fabric.Builder(this)
                .kits(Crashlytics())
                .build()
        Fabric.with(fabric)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}