package ws.arete.clientemobility.util

import android.text.Spannable
import android.text.SpannableString
import android.text.style.TypefaceSpan
import androidx.appcompat.app.ActionBar

/**
 * @author Created by normansanchez on 6/26/19.
 */
class ActivityUtils {
    companion object {
        @JvmStatic
        fun addFragmentToActivity(fragmentManager: androidx.fragment.app.FragmentManager, fragment: androidx.fragment.app.Fragment,
                                  frameId: Int) {
            checkNotNull(fragmentManager)
            checkNotNull(fragment)
            val transaction = fragmentManager.beginTransaction()
            transaction.add(frameId, fragment)
            transaction.commit()
        }

        @JvmStatic
        fun setTitle(actionBar: ActionBar?, title: String) {
            val s = SpannableString(title)
            s.setSpan(TypefaceSpan("sans-serif-regular"), 0, s.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            actionBar?.title = s
        }
    }
}