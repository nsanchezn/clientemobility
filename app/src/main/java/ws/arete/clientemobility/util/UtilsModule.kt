package ws.arete.clientemobility.util

import android.app.AppOpsManager
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.telephony.TelephonyManager
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import ws.arete.clientemobility.BuildConfig
import javax.inject.Singleton

/**
 * @author Created by normansanchez on 6/26/19.
 */
@Module
class UtilsModule {

    @Singleton
    @Provides
    internal  fun provideTelephonyManager(context: Application): TelephonyManager {
        return context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    }

    /*@Singleton
    @Provides
    internal  fun provideDataUseManager(networkStatsManager: Any?,
                                        telephonyManager: TelephonyManager,
                                        packageManager: PackageManager,
                                        viajesDataRepository: ViajesDataRepository,
                                        checaPermisos: ChecaPermisos): DataUseManager {
        return DataUseManager(networkStatsManager,telephonyManager,packageManager,
                viajesDataRepository,checaPermisos)
    }*/

    @Singleton
    @Provides
    internal  fun provideNetworkStatsManager(context: Application): Any? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.getSystemService(Context.NETWORK_STATS_SERVICE)
        } else {
            null
        }
    }

    @Singleton
    @Provides
    internal  fun providePackageManager(context: Application): PackageManager {
        return context.packageManager
    }

    @Singleton
    @Provides
    internal  fun appOpsManager(context: Application): AppOpsManager {
        return context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
    }

    /*@Singleton
    @Provides
    internal  fun provideTelefonoManager(telephonyManager: TelephonyManager, checaPermisos: ChecaPermisos): TelefonoManager {
        return TelefonoManager(telephonyManager,checaPermisos)
    }

    @Singleton
    @Provides
    internal fun provideStringsUtils(userDataRepository: UserDataRepository): StringsUtils {
        return StringsUtils(userDataRepository)
    }*/

    /*@Singleton
    @Provides
    internal  fun provideChecaPermisos(context: Application, appOpsManager: AppOpsManager): ChecaPermisos {
        return ChecaPermisos(context,appOpsManager)
    }

    @Singleton
    @Provides
    internal  fun provideGlobalSettingsProvider(contentResolver: ContentResolver, locationManager: LocationManager): GlobalSettingsProvider {
        return GlobalSettingsProvider(contentResolver,locationManager)
    }*/

    @Singleton
    @Provides
    internal  fun provideContentResolver(context: Application): ContentResolver {
        return context.contentResolver
    }

    @Singleton
    @Provides
    internal  fun provideLocationManager(context: Application): LocationManager {
        return context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    /*@Singleton
    @Provides
    internal  fun provideDateProvider(): DateProvider {
        return DateProvider()
    }*/

    @Singleton
    @Provides
    internal  fun provideFusedLocationProviderClient(context: Application): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }

    @Singleton
    @Provides
    internal  fun provideBackPressedCounter(context: Application): BackPressedCounter {
        return BackPressedCounter(context)
    }

    @Singleton
    @Provides
    internal fun provideGeoClient(context: Application): GeofencingClient {
        return LocationServices.getGeofencingClient(context)
    }

    @Singleton
    @Provides
    internal fun provideNotificationCompatBuilder(context: Application): NotificationCompat.Builder {

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Mobility"
            val descriptionText = "Canal de notificaciones Mobility"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(BuildConfig.APPLICATION_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            NotificationCompat.Builder(context,channel.id)
        }else{
            NotificationCompat.Builder(context,"")
        }
    }
}