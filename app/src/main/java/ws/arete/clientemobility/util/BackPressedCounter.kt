package ws.arete.clientemobility.util

import android.app.Application
import android.content.Intent
import android.widget.Toast
import androidx.core.content.ContextCompat
import javax.inject.Inject

private const val INTERVALO = 2000

/**
 * @author Created by normansanchez on 6/26/19.
 */
class BackPressedCounter @Inject constructor(private val context: Application) {

    private var mLastPressed: Long = 0

    fun checkLastPress(){
        if (mLastPressed + INTERVALO > System.currentTimeMillis()) {
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            ContextCompat.startActivity(context.baseContext, startMain, null)
        } else {
            Toast.makeText(context.baseContext, "Oprima de nuevo para salir de la aplicación", Toast.LENGTH_SHORT).show()
        }

        mLastPressed = System.currentTimeMillis()
    }
}