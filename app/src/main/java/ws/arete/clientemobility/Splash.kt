package ws.arete.clientemobility

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ws.arete.clientemobility.login.LoginActivity

/**
 * @author Created by normansanchez on 6/26/19.
 */
class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        startActivity(Intent(baseContext, LoginActivity::class.java))
        finish()
    }
}