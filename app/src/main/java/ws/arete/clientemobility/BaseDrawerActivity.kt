package ws.arete.clientemobility

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import javax.inject.Inject

/**
 * @author Created by normansanchez on 6/26/19.
 */
abstract class BaseDrawerActivity : DaggerAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{
    /*@Inject
    private lateinit var toggle: ActionBarDrawerToggle*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        if (supportActionBar == null) {
            setSupportActionBar(toolbar)
        }
        /*toggle = object : ActionBarDrawerToggle(
                this,
                drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            override fun onDrawerOpened(drawerView: View) {
                drawer_layout
                        .setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNDEFINED, GravityCompat.START)
            }

            override fun onDrawerClosed(drawerView: View) {
                drawer_layout
                        .setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_OPEN, GravityCompat.START)
                drawer_layout.closeDrawer(drawerView, true)
            }
        }
        drawer_layout.addDrawerListener(toggle)*/
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        nav_view.setNavigationItemSelectedListener(this)
        setDrawerInfo(nav_view.getHeaderView(0))
    }

    private fun setDrawerInfo(headerView: View) {
//        val userDataRepository = userDataRepositoryProvider.get()
//        Picasso.with(baseContext)
//                .load(R.drawable.icn_user_profile)
//                .transform(CircleTransformation())
//                .resize(130, 130)
//                .centerInside()
//                .into(headerView.findViewById<ImageView>(R.id.nav_bar_profilePic))
//        headerView.findViewById<TextView>(R.id.nav_bar_nombre).text = userDataRepository.getNombre()
//        headerView.findViewById<TextView>(R.id.nav_bar_correo).text = userDataRepository.getCorreo()
    }
}