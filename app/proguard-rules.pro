# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/genma/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:
-keep class com.amazonaws.** { *; }
-keepnames class com.amazonaws.** { *; }
-dontwarn com.amazonaws.**
-dontwarn com.fasterxml.**
-dontwarn com.squareup.picasso.**
-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8
-keep class ws.arete.mobility4.viewHolders.** { *; }
-keep class ws.arete.mobility4.model.** { *; }
-keep class ws.arete.mobility4.login.retrofit.** { *; }
-keep class ws.arete.mobility4.detalles.retrofit.** { *; }
-keep class ws.arete.mobility4.incidentesTipo2.retrofit.** { *; }
-keep class ws.arete.mobility4.ingresotoken.retrofit.** { *; }
-keep class ws.arete.mobility4.liquidacionviaje.retrofit.** { *; }
-keepnames class ws.arete.mobility4.model.** { public *** get*();
                                                 public void set*(***);
                                                  *;}
-dontwarn com.google.errorprone.annotations.**
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
